# My project's README

Database structure DDL

Below tables are created in the MySQL database.

mysql> create table DEALS.deals (Deal_Name varchar (100),Local_Currency varchar (100),Fund varchar (100),Deal_ID int NOT NULL,PRIMARY KEY (Deal_ID));
Query OK, 0 rows affected (0.06 sec)

mysql> create table DEALS.CashFlows (CF_ID int NOT NULL,Deal_ID int NOT NULL, Value_Date  date,CF_Type  varchar(100),Cashflows decimal(10,5),PRIMARY KEY(CF_ID),FOREIGN KEY(Deal_ID) REFERENCES Deals(Deal_ID));
Query OK, 0 rows affected (0.05 sec)


Webserver

Apache web server is used in the application.

/etc/apache2/ httpd.conf – File to add the AddType

apachectl restart – Apache Server restart

File Structure

Application is using the php screens. All the php files are placed in the following location.

File Location: /Library/WebServer/Documents 

This screen will perform the following steps

•	Firstly, all the records in the deals and CashFlows tables will be displayed.
•	Insert rows into the deals and CashFlow tables.
•	Update the random columns in the deals and CashFlow tables.
•	Delete the specific deals and Cashflows rows from the table.

Please see the How To Use document for more screen shots for more details.




































